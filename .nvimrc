set nocompatible  " be iMproved, required
filetype off      " required

filetype plugin indent on
" show existing tab with 4 spaces width
set tabstop=4
" when indenting with '>', use 4 spaces width
set shiftwidth=4
" On pressing tab, insert 4 spaces
set expandtab

" save swap files separately from files being edited
set swapfile
set dir=~/temp/vim-swp

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'
" CSSComb
Plugin 'git://github.com/csscomb/vim-csscomb.git'
" Pug Syntax Highlighting
Plugin 'digitaltoad/vim-pug'
" ejs syntax highlighting
Plugin 'briancollins/vim-jst'
" Dracula Style
Plugin 'dracula/vim'
" NeoComplete
Plugin 'git://github.com/Shougo/neocomplete.vim'
" Fugitive
Plugin 'tpope/vim-fugitive'
" Colorizer
Plugin 'chrisbra/Colorizer'
" Command-T
Plugin 'git://git.wincent.com/command-t.git'
" Solarized Theme
Plugin 'altercation/vim-colors-solarized'
" Emmet for Vim
Plugin 'mattn/emmet-vim'
" airline Plugin + Themes
Plugin 'bling/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
" Dev icons
Plugin 'ryanoasis/vim-devicons'
" NERDtree Plugin
Plugin 'scrooloose/nerdtree'
" Syntastic
Plugin 'scrooloose/syntastic'
" NERDtreeTabs
Plugin 'jistr/vim-nerdtree-tabs'
" Twig syntax highlighting
Plugin 'evidens/vim-twig'
" gitgutter plugin to show git diff in gutter
Plugin 'airblade/vim-gitgutter'
" Tmux-Navigator
Plugin 'christoomey/vim-tmux-navigator'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

" autostart NERDtree
let g:nerdtree_tabs_open_on_console_startup=1
" autoselect open file in NERDtree
let g:nerdtree_tabs_autofind=1

syntax on
set laststatus=2
set number
set t_Co=256
set background=dark
set wrap nowrap
colorscheme solarized
"colorscheme dracula
"colorscheme materialtheme
"colorscheme monokai

" Transparency fix
hi Normal ctermbg=none

set encoding=utf-8

if !exists('g:airline_symbols')
	let g:airline_symbols = {}
endif

let g:airline_powerline_fonts = 1
set guifont=DejaVu\ Sans\ Mono\ for\ Powerline\ Book\ 11
let g:airline_theme='solarized'
let g:solarized_termcolors=256

" Toogle NERDTree with C-e
nnoremap <C-e> :NERDTreeToggle<CR>

" Move vertically by visual line
nnoremap j gj
nnoremap k gk

" Gitgutter Config
let g:gitgutter_max_signs=9999

" Syntastic Config
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

" CSSLint Syntastic Config
let g:syntastic_csslint_args = "--ignore=outline-none"

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_loc_list_height = 10

" NeoComplete
"Note: This option must be set in .vimrc(_vimrc).  NOT IN .gvimrc(_gvimrc)!
" Disable AutoComplPop.
let g:acp_enableAtStartup = 0
" Use neocomplete.
let g:neocomplete#enable_at_startup = 1
" Use smartcase.
let g:neocomplete#enable_smart_case = 1
" Set minimum syntax keyword length.
let g:neocomplete#sources#syntax#min_keyword_length = 3
let g:neocomplete#lock_buffer_name_pattern = '\*ku\*'

" Define dictionary.
let g:neocomplete#sources#dictionary#dictionaries = {
    \ 'default' : '',
    \ 'vimshell' : $HOME.'/.vimshell_hist',
    \ 'scheme' : $HOME.'/.gosh_completions'
        \ }

" Define keyword.
if !exists('g:neocomplete#keyword_patterns')
    let g:neocomplete#keyword_patterns = {}
endif
let g:neocomplete#keyword_patterns['default'] = '\h\w*'

" Plugin key-mappings.
inoremap <expr><C-g>     neocomplete#undo_completion()
inoremap <expr><C-l>     neocomplete#complete_common_string()

" Recommended key-mappings.
" <CR>: close popup and save indent.
inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
function! s:my_cr_function()
  return (pumvisible() ? "\<C-y>" : "" ) . "\<CR>"
  " For no inserting <CR> key.
  "return pumvisible() ? "\<C-y>" : "\<CR>"
endfunction
" <TAB>: completion.
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
" <C-h>, <BS>: close popup and delete backword char.
inoremap <expr><C-h> neocomplete#smart_close_popup()."\<C-h>"
inoremap <expr><BS> neocomplete#smart_close_popup()."\<C-h>"
" Close popup by <Space>.
"inoremap <expr><Space> pumvisible() ? "\<C-y>" : "\<Space>"

" AutoComplPop like behavior.
"let g:neocomplete#enable_auto_select = 1

" Shell like behavior(not recommended).
"set completeopt+=longest
"let g:neocomplete#enable_auto_select = 1
"let g:neocomplete#disable_auto_complete = 1
"inoremap <expr><TAB>  pumvisible() ? "\<Down>" : "\<C-x>\<C-u>"

" Enable omni completion.
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags

" Enable heavy omni completion.
if !exists('g:neocomplete#sources#omni#input_patterns')
  let g:neocomplete#sources#omni#input_patterns = {}
endif
"let g:neocomplete#sources#omni#input_patterns.php = '[^. \t]->\h\w*\|\h\w*::'
"let g:neocomplete#sources#omni#input_patterns.c = '[^.[:digit:] *\t]\%(\.\|->\)'
"let g:neocomplete#sources#omni#input_patterns.cpp = '[^.[:digit:] *\t]\%(\.\|->\)\|\h\w*::'

" For perlomni.vim setting.
" https://github.com/c9s/perlomni.vim
let g:neocomplete#sources#omni#input_patterns.perl = '\h\w*->\h\w*\|\h\w*::'
